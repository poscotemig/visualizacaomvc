import React, { Component } from "react";
import { Text, View } from "react-native";
import Controller from "./HomeController";
import { Container, Header, Content, Form, Picker } from "native-base";
import styles from "./styles";
import { observer } from "mobx-react";

@observer
class Home extends Component {
  componentWillMount() {
    Controller.definirFormaDeExibicao();
  }

  render() {
    return (
      <Container>
        <Header />
        <Content>
          <Form>
            <Picker
              note
              mode="dropdown"
              style={{ width: 120 }}
              selectedValue={Controller.selected}
              onValueChange={text => Controller.onValueChange(text)}
            >
              {Controller.valores.map((item, index) => {
                return (
                  <Picker.Item key={index} label={item.nome} value={index} />
                );
              })}
            </Picker>
          </Form>
          {Controller.formaDeExibir == 0 ? (
            <Text>Lista</Text>
          ) : Controller.formaDeExibir == 1 ? (
            <Text>Grid</Text>
          ) : (
            <Text>Carrosel</Text>
          )}
        </Content>
      </Container>
    );
  }
}

export default Home;

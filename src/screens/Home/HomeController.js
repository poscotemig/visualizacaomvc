import { observable, action } from "mobx";

class Controller {
  @observable
  selected = 0;

  @observable
  formaDeExibir;

  valores = [
    { nome: "Minas Gerais", tipo: 0 },
    { nome: "Rio de Janeiro", tipo: 1 },
    { nome: "São Paulo", tipo: 2 }
  ];

  @action
  onValueChange(value) {
    this.selected = value;
    this.definirFormaDeExibicao();
  }

  @action
  definirFormaDeExibicao() {
    this.formaDeExibir = this.valores[this.selected].tipo;
  }
}

export default new Controller();
